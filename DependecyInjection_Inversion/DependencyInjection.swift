//
//  DependencyInjection.swift
//  DependecyInjection_Inversion
//
//  Created by Pooja Raghuwanshi on 02/03/23.
//

import UIKit

class DependencyInjection: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let obj = Dependent(dependetObj: Dependentcls())
        obj.creatReq()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class Dependentcls {
    
    func askMemory(){
        
        print("Saved data")
    }
}

class Dependent{
    
    
    var dependetObj :Dependentcls?
    
    init(dependetObj: Dependentcls) {
        self.dependetObj = dependetObj
    }
    
    func creatReq(){
        
        dependetObj?.askMemory()
    }
}
