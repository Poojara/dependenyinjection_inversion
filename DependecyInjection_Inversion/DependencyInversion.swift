//
//  DependencyInversion.swift
//  DependecyInjection_Inversion
//
//  Created by Pooja Raghuwanshi on 02/03/23.
//

import UIKit

class DependencyInversion: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let obj = SaveStorge(obj: SaveLocal())
        obj.saveData(order: Order(orderName: "pen", orderPrice: 20.0))

        let objs = SaveStorge(obj: SaveAPi())
        objs.saveData(order: Order(orderName: "pen", orderPrice: 20.0))

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
struct Order{
    
    let orderName : String
    let orderPrice : Float
}

protocol StorageObject{
    
    func save(order:Order)
}

class SaveLocal : StorageObject{
    
    
    func  save(order:Order){
        
        print("saved Local data")
    }
}

class SaveAPi : StorageObject{
    
    
    func  save(order:Order){
        
        print("saved Api data")
    }
}

class SaveStorge {
    
    var obj : StorageObject?
    
    init(obj: StorageObject) {
        self.obj = obj
    }
    func saveData(order: Order){
        
        obj?.save(order: order)
    }
    
}
