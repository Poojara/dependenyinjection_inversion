//
//  ViewController.swift
//  DependecyInjection_Inversion
//
//  Created by Pooja Raghuwanshi on 02/03/23.
//

import UIKit
/*
dependency injection is a technique whereby one object supplies the dependencies of another object

 dependency inversion
 
 It says modules should depend on the contract instead of each other. This contract determines what goes out of the higher module and what goes into the lower module, and it guarantees both modules that the communication will be done according to its terms.24-Feb-2021

 
*/
